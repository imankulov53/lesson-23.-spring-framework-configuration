package com.controller;

import com.model.Order;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/order")
public interface OrderController {


    //Получает список всех ордеров
    @GetMapping
    String listAll(Model model);

    //Получает запрос на добавление, в виду чего переводит на форму добавления
    @GetMapping("/new")
    String newFrom();

    @GetMapping("/sort_by/{mode}")
    String sortPrice(@PathVariable("mode") String mode,Model model);

//    @GetMapping("/sort_by/asc")
//    String sortAsc(Model model);


    @PostMapping("/update")
    String update(Order order,Model model);

    //Получает запрос на изменение, в виду чего переводит на форму изменения
    @GetMapping("/update/{id}")
    String updateForm(@PathVariable("id") Integer id,Model model);

    //Происходит сохранения обьекта
    @PostMapping
    String save(Order order, Model model);

    //Происходит удаление объекта
    @GetMapping("/delete/{id}")
    String delete(@PathVariable Integer id, Model model);
}
