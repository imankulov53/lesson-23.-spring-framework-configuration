package com.controller;


import com.model.Order;
import com.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


@Controller
public class OrderControllerIml implements OrderController {

    //Здесь реализуй поведение по запросам

    @Autowired
    private OrderService service;

    //Получает список всех ордеров и выводит его
    @Override
    public String listAll(Model model) {
     return allOrders(model);
    }

    //Получает запрос на добавление, в виду чего переводит на форму добавления
    @Override
    public String newFrom() {
        return "putUpdate";
    }

    @Override
    public String sortPrice(@PathVariable("mode") String mode, Model model) {

        if(mode.equals("desc")){
            List<Order> list = service.listAll().stream().
                    sorted(Comparator.comparingDouble(Order::getPrice).reversed()).collect(Collectors.toList());
            model.addAttribute("orders", list);
        }else {
            if (mode.equals("asc")) {
                List<Order> list = service.listAll().stream().
                        sorted(Comparator.comparingDouble(Order::getPrice)).collect(Collectors.toList());
                model.addAttribute("orders", list);
            }
        }
        return "view";
    }

//    @Override
//    public String sortAsc(Model model) {
//        List<Order> list = service.listAll().stream().
//                sorted(Comparator.comparingDouble(Order::getPrice)).collect(Collectors.toList());
//        model.addAttribute("orders", list);
//        return "view";
//    }

    @Override
    public String update(Order order, Model model)
    {
        String check = checkingAllParam(order,model);
        if (check==null){
            check = "redirect:/order";
            //Здесь происходит сохранение объекта
            service.update(order);
        }
        //Вывод всех объектов
        return check;
    }

    //Получает запрос на изменение, в виду чего переводит на форму изменения
    @Override
    public String updateForm(@PathVariable("id") Integer id, Model model)
    {
        if (service.getOne(id)!=null){
            Order order = service.getOne(id);
            model.addAttribute("order",order);
            return "update";
        }else {
            model.addAttribute("message","You trying to modify order that not existed");
            model.addAttribute("path","/order");
            return "error";
        }
    }

    //Происходит сохранения обьекта
    @Override
    public String save( Order order, Model model) {
        //Происходит чек всех параметров
        String check = checkingAllParam(order,model);
        if (check==null){
            check = "redirect:/order";
            //Здесь происходит сохранение объекта
            service.save(order);
        }
            //Вывод всех объектов
            return check;

    }
    @Override
    public String delete(Integer id, Model model) {
        //Здесь происходит удаление объекта
        service.delete(id);
        //Вывод всех объектов
        return "redirect:/order";
    }


    /*------------------------------------------------*/
    private String allOrders(Model model){
        List<Order> list = service.listAll();
        model.addAttribute("orders", list);
        return "view";
    }

    private String checkingAllParam(Order order, Model model){
        if (order.getNameClient().isBlank() || order.getNameClient()==null || order.getNameClient().isEmpty() || order.getNameClient().length()==0){
            model.addAttribute("message","You haven't insert the name of Client");
            model.addAttribute("path","/order/new");
            return "error";
        }
        if (order.getPrice()<=0.00){
            model.addAttribute("message","The Price cannot been lower than zero");
            model.addAttribute("path","/order/new");
            return "error";
        }
        if (order.getNumber()<=0){
            model.addAttribute("message", "the Number cannot been lower than zero");
            model.addAttribute("path","/order/new");
            return "error";
        }
        return null;
    }
}
