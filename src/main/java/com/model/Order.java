package com.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name= "orders")
public class Order {
        //Добваь все сущности Hibernate и sequences
    @Id
    @Column(name = "id")
    @SequenceGenerator(name = "id_order", sequenceName = "id_order", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_order")
    private int id;

    @Column(name = "name_client")
    private String nameClient;

    @Column(name = "price")
    private double price;

    @Column(name = "nums")
    private int number;

    @Column(name = "dates")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date;

    public Order(int id, String nameClient, double price, int number, Date date) {
        this.id = id;
        this.nameClient = nameClient;
        this.price = price;
        this.number = number;
        this.date = date;
    }

    public Order() {
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", nameClient='" + nameClient + '\'' +
                ", price=" + price +
                ", number=" + number +
                ", date=" + date +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameClient() {
        return nameClient;
    }

    public void setNameClient(String nameClient) {
        this.nameClient = nameClient;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
