package com.service;

import com.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.store.OrderRepository;

import java.util.List;

@Service
public class OrderService {
    //Здесь вроде как ничего не нужно добавлять, это уже является обслуживащим классом
    @Autowired
    private OrderRepository repository;

    public List<Order> listAll() {
        return repository.listAll();
    }

    public Order getOne(Integer id) {
        return repository.getOne(id);
    }

    public void save(Order employee) {
        repository.save(employee);
    }

    public void delete(Integer id) {
        repository.delete(id);
    }

    public void update(Order order){repository.update(order);}

}
