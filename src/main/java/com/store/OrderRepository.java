package com.store;

import com.model.Order;

import java.util.List;

public interface OrderRepository {
    List<Order> listAll();

    Order getOne(Integer id);

    void save(Order order);

    void delete(Integer id);

    void update(Order order);
}
