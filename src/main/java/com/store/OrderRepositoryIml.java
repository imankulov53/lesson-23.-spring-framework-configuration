package com.store;

import com.Util.HibernateUtil;
import com.model.Order;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderRepositoryIml implements OrderRepository{

    private Session currentSession;

    private Transaction currentTransaction;

    public Session openSession() {
        currentSession = HibernateUtil.getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeSession() {
        currentTransaction.commit();
        currentSession.close();
    }

    public Session getCurrentSession() {
        return currentSession;
    }


    //Здесь должна быть реализация похода в базу, но на всякий случай посмотри еще DAO

    @Override
    public List<Order> listAll() {
        openSession();
        List<Order> list = getCurrentSession().createQuery("from Order").getResultList();
        closeSession();
        return list;
    }

    @Override
    public Order getOne(Integer id) {
        openSession();
        Order order = getCurrentSession().get(Order.class,id);
        closeSession();
        return order;
    }

    @Override
    public void save(Order order) {
        openSession();
        getCurrentSession().persist(order);
        closeSession();
    }

    @Override
    public void delete(Integer id) {
        openSession();
        getCurrentSession().delete(getCurrentSession().get(Order.class,id));
        closeSession();
    }

    @Override
    public void update(Order order) {
        openSession();
        getCurrentSession().update(order);
        closeSession();
    }
}
