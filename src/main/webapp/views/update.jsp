<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 29.04.2021
  Time: 22:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update page</title>
</head>
<body>
<form action="<%=request.getContextPath()%>/order/update" method="post">

  <input type="hidden" name="id" value="${order.getId()}">

  <table>
    <tr>
      <td>Name:</td>
      <td><input type="text" name="nameClient" value="${order.getNameClient()}" required></td>
    </tr>
    <tr>
      <td>Price:</td>
     <td><input type="number" name="price" step="0.01" value="${order.getPrice()}" required></td>
    </tr>
    <tr>
      <td>Number:</td>
      <td><input type="number" name="number" value="${order.getNumber()}" required></td>
    </tr>
    <tr>
      <td>Date:</td>
      <td><input type="date" name="date" value="${order.getDate()}" required></td>
    </tr>
  </table>

  <input type="submit" name="Update">
</form>
</body>
</html>
