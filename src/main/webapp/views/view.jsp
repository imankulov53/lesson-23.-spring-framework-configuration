<%@ page import="java.util.List" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<html>
<head>
    <title>All orders</title>
</head>
<body>

<h1> List of all orders</h1>

<button><a href="<%=request.getContextPath()%>/order/new">Add</a></button>
<button><a href="<%=request.getContextPath()%>/order/sort_by/desc">Sort price by Desc</a></button>
<button><a href="<%=request.getContextPath()%>/order/sort_by/asc">Sort price by Asc</a></button>
<table style="width:100%" border="1">
    <tr>
        <td>ID</td>
        <td>Name</td>
        <td>Price</td>
        <td>Number</td>
        <td>Date</td>
        <td>Update</td>
        <td>Delete</td>
    </tr>

    <c:forEach var="order" items="${orders}">
        <tr>
            <td>${order.getId()}</td>
            <td>${order.getNameClient()}</td>
            <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${order.getPrice()}"/></td>
            <td>${order.getNumber()}</td>
            <td><fmt:formatDate value="${order.getDate()}" pattern="yyyy/MM/dd" /></td>
            <td><button><a href=<%=request.getContextPath()%>/order/update/${order.getId()}>Update</a></button></td>
            <td><button><a href=<%=request.getContextPath()%>/order/delete/${order.getId()}>Delete</a></button></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
